#ifndef CutsAW_DriftTimeETrainStudy_H
#define CutsAW_DriftTimeETrainStudy_H

#include "EventBase.h"

class CutsAW_DriftTimeETrainStudy {

public:
    CutsAW_DriftTimeETrainStudy(EventBase* eventBase);
    ~CutsAW_DriftTimeETrainStudy();
    bool AW_DriftTimeETrainStudyCutsOK();
    bool AboveAnode( double s1TBA, double s2TBA );
    bool CX_BottomLeftPathology( double logS1, double logS2 );
    bool CX_TopLeftPathology( double logS1, double logS2 );
    bool CX_MiddleLeftBand( double logS1, double logS2 );
    bool CX_FiducialVolumeR( const float rS2, const float driftTime );
    bool CX_FiducialVolumeZ(double driftTime_ns);
    bool CX_FiducialVolume(const float rS2, const float driftTime_ns);
    int  GatherPodChannelInfo();
    std::pair<unsigned long, unsigned long> ComputeCountStart(std::pair<unsigned long, unsigned long> eventTime, unsigned long preTrigTime_ns);
      std::pair<unsigned long, unsigned long> ComputeCountEnd(std::pair<unsigned long, unsigned long> eventTime, unsigned long postTrigTime_ns);
      unsigned long long ComputeDeltaT(std::pair<unsigned long, unsigned long> firstTime, std::pair<unsigned long, unsigned long> secondTime );
private:
    EventBase* m_event;
};

#endif
