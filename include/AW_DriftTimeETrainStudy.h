#ifndef AW_DriftTimeETrainStudy_H
#define AW_DriftTimeETrainStudy_H

#include "Analysis.h"

#include "CutsAW_DriftTimeETrainStudy.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class AW_DriftTimeETrainStudy : public Analysis {

public:
    AW_DriftTimeETrainStudy();
    ~AW_DriftTimeETrainStudy();

    void Initialize();
    void Execute();
    void Finalize();

protected:
    CutsAW_DriftTimeETrainStudy* m_cutsAW_DriftTimeETrainStudy;
    ConfigSvc* m_conf;
};

#endif
