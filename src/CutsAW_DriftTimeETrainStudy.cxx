#include "CutsAW_DriftTimeETrainStudy.h"
#include "ConfigSvc.h"
#include "TMath.h"

CutsAW_DriftTimeETrainStudy::CutsAW_DriftTimeETrainStudy(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsAW_DriftTimeETrainStudy::~CutsAW_DriftTimeETrainStudy()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsAW_DriftTimeETrainStudy::AW_DriftTimeETrainStudyCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}

bool CutsAW_DriftTimeETrainStudy::AboveAnode( double s1TBA, double s2TBA )
{
  return (s1TBA > -0.65 && s2TBA > 0.45);
}

bool CutsAW_DriftTimeETrainStudy::CX_FiducialVolumeR( const float rS2, const float driftTime )
{
  const std::array<double,7> m_idealFiducialWallFit = { 71.351, 0.0175366, 4.51307e-5, -8.07743e-9, -2.14047e-10, -3.17783e-13, -1.44533e-16 };
  
  double boundaryR = 0;
  for( size_t i = 0; i < m_idealFiducialWallFit.size(); ++i ){
    boundaryR += m_idealFiducialWallFit.at(i)*TMath::Power(-driftTime,(double)i);
  }
  boundaryR -= 4;
  if( rS2 > boundaryR ) return false;
  return true;
}

bool CutsAW_DriftTimeETrainStudy::CX_FiducialVolume(const float rS2, const float driftTime_ns)
{
  double driftTime_us = ((double)driftTime_ns / 1000.);
  return CX_FiducialVolumeZ(driftTime_us) && CX_FiducialVolumeR(rS2,driftTime_us);
}

bool CutsAW_DriftTimeETrainStudy::CX_FiducialVolumeZ(double driftTime_ns)
{
  const double m_idealFiducialLowerBoundary_us = 936.5;
  const double m_idealFiducialUpperBoundary_us = 86;
  if( driftTime_ns > m_idealFiducialLowerBoundary_us ) {
    return false;
  }
  if( driftTime_ns < m_idealFiducialUpperBoundary_us ){
    return false;
  }
  return true;
}

bool CutsAW_DriftTimeETrainStudy::CX_BottomLeftPathology( double logS1, double logS2 )
{
  if( logS1 < 1.0 &&
      logS2 < 2.5 ) return true;
  return false;
}

bool CutsAW_DriftTimeETrainStudy::CX_TopLeftPathology( double logS1, double logS2 )
{
  if( logS1 < 1.0 &&
      logS2 > 5.0 ) return true;
  return false;
}

bool CutsAW_DriftTimeETrainStudy::CX_MiddleLeftBand( double logS1, double logS2 )
{
  if( logS1 < 1.0 &&
      logS2 > 2.5 && logS2 < 5.0 ) return true;
  return false;
}

int CutsAW_DriftTimeETrainStudy::GatherPodChannelInfo()
{
  int nChannelsFailingPodQualityCut = 0;
  std::vector<int> nSamplesInChannel_vect = (*m_event->m_tpcEvent)->nSamplesInChannel;
  for( int iC = 0; iC < nSamplesInChannel_vect.size(); ++iC ){
    if( nSamplesInChannel_vect[iC] >= 18000 ){
      nChannelsFailingPodQualityCut++;
    }
  }
  return nChannelsFailingPodQualityCut;
}
std::pair<unsigned long, unsigned long> CutsAW_DriftTimeETrainStudy::ComputeCountStart(std::pair<unsigned long, unsigned long> eventTime, unsigned long preTrigTime_ns)
{
  unsigned long startTime_s  = eventTime.first;
  unsigned long startTime_ns = 0;

  if (fabs(preTrigTime_ns) > eventTime.second){
    startTime_s -= 1;
    startTime_ns = 1e9 - (preTrigTime_ns - eventTime.second);
  }
  else
    startTime_ns = eventTime.second - preTrigTime_ns;
  return std::make_pair(startTime_s,startTime_ns);
}
std::pair<unsigned long, unsigned long> CutsAW_DriftTimeETrainStudy::ComputeCountEnd(std::pair<unsigned long, unsigned long> eventTime, unsigned long postTrigTime_ns)
{
  unsigned long endTime_s    = eventTime.first;
  unsigned long endTime_ns   = 0;

  if (postTrigTime_ns < 0 and fabs(postTrigTime_ns) > eventTime.second){
    endTime_s -= 1;
    endTime_ns = 1e9 - (fabs(postTrigTime_ns) - eventTime.second);
  }
  else if( eventTime.second + postTrigTime_ns >= 1e9 ){
    endTime_s += 1;
    endTime_ns = eventTime.second + postTrigTime_ns - 1e9;
  }
  else
    endTime_ns = eventTime.second + postTrigTime_ns;

  return std::make_pair(endTime_s,endTime_ns);
}

//////////////////////////////////////////////////////////////////////////////////////////
// Compute the time difference between the first pulse and second pulse
// Mod. version from SR1 Core Cuts to return unsigned long long 
//---------------------------------------------------------------------------------------
unsigned long long CutsAW_DriftTimeETrainStudy::ComputeDeltaT(std::pair<unsigned long, unsigned long> firstTime, std::pair<unsigned long, unsigned long> secondTime )
{
  unsigned long t1_s = firstTime.first;
  unsigned long t1_ns = firstTime.second;
  unsigned long t2_s = secondTime.first;
  unsigned long t2_ns = secondTime.second;
  
  unsigned long deltaT_s = t2_s - t1_s;
  unsigned long deltaT_ns = 0;
  if( t2_ns >= t1_ns ){
    deltaT_ns = t2_ns - t1_ns;
  }
  else{
    deltaT_s -= 1;
    deltaT_ns = 1e9 - (t1_ns - t2_ns);
  }
  
  unsigned long long output = deltaT_ns + deltaT_s*1e9;
  return output;
}
