#include "AW_DriftTimeETrainStudy.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsAW_DriftTimeETrainStudy.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"


std::pair<unsigned long, unsigned long> last_s2_start = std::make_pair(0,0);
std::vector< std::pair<unsigned long, unsigned long> > isolated_s2_aft1_times;
std::vector< std::pair<unsigned long, unsigned long> > isolated_s2_s1_aft1_times;
std::vector< std::pair<unsigned long, unsigned long> > isolated_s2_aft99_times;
std::vector< float > isolated_s2_drift_times;
std::vector< float > isolated_s2_pulse_areas;
std::vector< float > isolated_s2_xs;
std::vector< float > isolated_s2_ys;
std::vector< float > isolated_s2_xs_centroid;
std::vector< float > isolated_s2_ys_centroid;
std::vector< bool >  isolated_s2_flags;

// vector of times 
std::map<int,float> prev_evt_sum_pa_start { {50,0.},{100,0.},{200,0.},{500,0},{1000,0.} };
std::map<int,float> prev_evt_sum_pa_end   { {50,0.},{100,0.},{200,0.},{500,0},{1000,0.} };
std::map<int,float> this_evt_sum_pa_start { {50,0.},{100,0.},{200,0.},{500,0},{1000,0.} };
std::map<int,float> this_evt_sum_pa_end   { {50,0.},{100,0.},{200,0.},{500,0},{1000,0.} };
std::map<int,float> ratios { {50,0.},{100,0.},{200,0.},{500,0},{1000,0.} };
std::map<int,float> ratios_sig { {50,0.},{100,0.},{200,0.},{500,0},{1000,0.} };

// vector of times 
std::map<int,int> prev_evt_npulse_start { {50,0},{100,0},{200,0},{500,0},{1000,0} };
std::map<int,int> prev_evt_npulse_end   { {50,0},{100,0},{200,0},{500,0},{1000,0} };
std::map<int,int> this_evt_npulse_start { {50,0},{100,0},{200,0},{500,0},{1000,0} };
std::map<int,int> this_evt_npulse_end   { {50,0},{100,0},{200,0},{500,0},{1000,0} };
std::map<int,float> ratios_npulse { {50,0.},{100,0.},{200,0.},{500,0},{1000,0.} };
std::map<int,float> ratios_npulse_sig { {50,0.},{100,0.},{200,0.},{500,0},{1000,0.} };

std::map<int,int> prev_evt_nveto_start  { {50,0},{100,0},{200,0},{500,0},{1000,0} };
std::map<int,int> prev_evt_nveto_end    { {50,0},{100,0},{200,0},{500,0},{1000,0} };
std::map<int,int> this_evt_nveto_start  { {50,0},{100,0},{200,0},{500,0},{1000,0} };
std::map<int,int> this_evt_nveto_end    { {50,0},{100,0},{200,0},{500,0},{1000,0} };

// Constructor
AW_DriftTimeETrainStudy::AW_DriftTimeETrainStudy()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("ss");
    m_event->IncludeBranch("ms");
    m_event->IncludeBranch("other");
    m_event->IncludeBranch("pileUp");
    m_event->IncludeBranch("pulsesTPC");
    m_event->IncludeBranch("eventTPC");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("AW_DriftTimeETrainStudy Analysis");

    // Setup the analysis specific cuts.
    m_cutsAW_DriftTimeETrainStudy = new CutsAW_DriftTimeETrainStudy(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
AW_DriftTimeETrainStudy::~AW_DriftTimeETrainStudy()
{
    delete m_cutsAW_DriftTimeETrainStudy;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void AW_DriftTimeETrainStudy::Initialize()
{
    INFO("Initializing AW_DriftTimeETrainStudy Analysis");
    m_cuts->sr1()->Initialize();
    
}

// Execute() - Called once per event.
void AW_DriftTimeETrainStudy::Execute()
{
  // Some settings
  double progenitorThresh         = 5000.; // in phd
  unsigned long vetoWindow        = 200  * pow(10,6); // 200 ms
  unsigned long long postS2Window = 20000 * pow(10,6); // 20 s, amount of time to save prog S2

  // std::vector<std::pair<double,double>> top_drift_slices = {std::make_pair(0.,  1.e5), 
  //                                                           std::make_pair(1.e5,2.e5), 
  //                                                           std::make_pair(3.e5,4.e5)};
  // std::vector<std::pair<double,double>> bot_drift_slices = {std::make_pair(5.e5,6.e5), 
  //                                                           std::make_pair(7.e5,8.e5), 
  //                                                           std::make_pair(9.e5,10.e5)};
  std::vector<std::pair<float,float>>   s2_log_pa_slices = { std::make_pair(3.7,4.5),
                                                             std::make_pair(4.5,5),
                                                             std::make_pair(5.,5.4),
                                                             std::make_pair(5.4,5.6),
                                                             std::make_pair(5.6,5.8),
                                                             std::make_pair(5.8,6)};
  double elifetime_us             = 7000.;
  float pulseArea_SE              = 50.;
  float maxdR_cm                  = 50.;

  // Calculate event times
  unsigned long runID        = (*m_event->m_eventHeader)->runID;
  unsigned long eventID      = (*m_event->m_eventHeader)->eventID;
  unsigned long trig_s       = (*m_event->m_eventHeader)->triggerTimeStamp_s;
  unsigned long trig_ns      = (*m_event->m_eventHeader)->triggerTimeStamp_ns;
  unsigned long pre_trig_ns  = (*m_event->m_eventHeader)->preTriggerWindow_ms*pow(10,6);
  unsigned long post_trig_ns = (*m_event->m_eventHeader)->postTriggerWindow_ms*pow(10,6);
  unsigned long daq_lv_s     = (*m_event->m_eventHeader)->daqLivetime_s;
  unsigned long daq_lv_ns    = (*m_event->m_eventHeader)->daqLivetime_ns;
  unsigned long long event_window_ns = pre_trig_ns + post_trig_ns;

  unsigned long long daq_lv_ns_all   = daq_lv_s*pow(10,9) + daq_lv_ns;

  std::pair<unsigned long,unsigned long> evt_start = m_cutsAW_DriftTimeETrainStudy->ComputeCountStart(std::make_pair(trig_s,trig_ns),
                                                                                                      pre_trig_ns);
  std::pair<unsigned long,unsigned long> evt_end   = m_cutsAW_DriftTimeETrainStudy->ComputeCountEnd(std::make_pair(trig_s,trig_ns),
                                                                                                    post_trig_ns);

  m_hists->BookFillHist("h_nEvents; evt_count",1,0.5,1.5,1.);
  double lt = 11.1*pow(10,-3);
  m_hists->BookFillHist("h_livetime; evt_count",1,0.5,1.5,1.,lt);
  
  // Loop through all TPC pulses 
  std::vector<int> pulseIDs = (*m_event->m_tpcPulses)->pulseID;
  int nPulses = pulseIDs.size();
  int SS_S2_pulseID = (*m_event->m_singleScatter)->s2PulseID; 
  int SS_S1_pulseID = (*m_event->m_singleScatter)->s1PulseID; 
  std::pair <unsigned long, unsigned long> s1_abs_aft1_time = m_cuts->sr1()->GenerateFullPulseTime(trig_s,trig_ns,(*m_event->m_tpcPulses)->areaFractionTime1_ns[SS_S1_pulseID] + (*m_event->m_tpcPulses)->pulseStartTime_ns[SS_S1_pulseID]);
  std::pair <unsigned long, unsigned long> s2_abs_aft1_time = m_cuts->sr1()->GenerateFullPulseTime(trig_s,trig_ns,(*m_event->m_tpcPulses)->areaFractionTime1_ns[SS_S2_pulseID] + (*m_event->m_tpcPulses)->pulseStartTime_ns[SS_S2_pulseID]);

  // Get all MS IDs
  int MS_S1_pulseID = (*m_event->m_multipleScatter)->s1PulseID; 
  std::vector<int> MS_S2_pulseIDs = (*m_event->m_multipleScatter)->s2PulseIDs; 
  
  // Total MS pulse area
  float MS_pulseArea = 0.;
  for (int ij = 0; ij < MS_S2_pulseIDs.size(); ij++){
    MS_pulseArea += (*m_event->m_tpcPulses)->pulseArea_phd[MS_S2_pulseIDs[ij]];
  }
  float s2other_nonProgenitorArea = 0.;

  for (int i = 0; i < nPulses; i++){
    // Get pulse information
    std::string classification = (*m_event->m_tpcPulses)->classification[i];
    float pulseArea            = (*m_event->m_tpcPulses)->pulseArea_phd[i];
    int   aft99                = (*m_event->m_tpcPulses)->areaFractionTime99_ns[i] + (*m_event->m_tpcPulses)->pulseStartTime_ns[i];
    int   aft1                 = (*m_event->m_tpcPulses)->areaFractionTime1_ns[i] + (*m_event->m_tpcPulses)->pulseStartTime_ns[i];
    float x_cm                 = (*m_event->m_tpcPulses)->s2Xposition_cm[i];
    float y_cm                 = (*m_event->m_tpcPulses)->s2Yposition_cm[i];
    float x_cm_centroid        = (*m_event->m_tpcPulses)->topCentroidX_cm[i];
    float y_cm_centroid        = (*m_event->m_tpcPulses)->topCentroidY_cm[i];
    std::pair <unsigned long, unsigned long> abs_aft1_time = m_cuts->sr1()->GenerateFullPulseTime(trig_s,trig_ns,aft1);
    std::pair <unsigned long, unsigned long> abs_aft99_time = m_cuts->sr1()->GenerateFullPulseTime(trig_s,trig_ns,aft99);
    float driftTime_ns = -1.; 
    bool save = false;

    unsigned long long dt_to_end   = m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(abs_aft1_time, evt_end);
    unsigned long long dt_to_start = m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(evt_start, abs_aft1_time);

    // try to do some map stuff
    for ( const auto &kv : prev_evt_sum_pa_start ) {
      int tw = kv.first;
      if (dt_to_start < tw * pow(10,3)){
        this_evt_sum_pa_start[tw] += pulseArea;
        this_evt_npulse_start[tw] ++;
      }
      if (dt_to_end < tw * pow(10,3)){
        this_evt_sum_pa_end[tw] += pulseArea;
        this_evt_npulse_end[tw] ++;
      }
    }
    
    // A vetoing pulse can be
    // - any S2, S1, or Other pulse
    if (classification != "S2" && classification != "Other" && classification != "S1")
      continue;

    // - pulse greater than threshold
    // - pulse part of a large MS which passes the threshold
    if (std::find(MS_S2_pulseIDs.begin(), MS_S2_pulseIDs.end(), i) == MS_S2_pulseIDs.end()){
      if ( pulseArea < progenitorThresh ){
        s2other_nonProgenitorArea += pulseArea;
        continue;
      }
    }
    else { // Part of MS
      if (MS_pulseArea < progenitorThresh){
        continue;
      }
    }
  }    
    float logProgArea = TMath::Log10(pulseArea);
    m_hists->BookFillHist("h_logProgenitorS2Area_noCuts; Log10(S2 Area [phd])",200,1.,7.,logProgArea);
    m_hists->BookFillHist("h_x_vs_y_noCuts; x [cm]; y [cm]",200,-100.,100.,200,-100.,100.,x_cm,y_cm);

    // now save pulses which are used in the rate plot
    // pulse must be an S2 associated to the SS
    if (i == SS_S2_pulseID) {
      //if (i == SS_S2_pulseID || std::find(MS_S2_pulseIDs.begin(), MS_S2_pulseIDs.end(), i) != MS_S2_pulseIDs.end()) {
      m_hists->BookFillHist("h_logProgenitorS2Area_SScut; Log10(S2 Area [phd])",200,1.,7.,logProgArea);
      m_hists->BookFillHist("h_x_vs_y_SScut; x [cm]; y [cm]",200,-100.,100.,200,-100.,100.,x_cm,y_cm);
      float s1TBA     = (*m_event->m_singleScatter)->s1TBA;
      float s2TopArea = (*m_event->m_singleScatter)->s2TopArea_phd;
      float s2BotArea = (*m_event->m_singleScatter)->s2BottomArea_phd;
      float s2TBA     = (s2TopArea-s2BotArea)/(s2TopArea+s2BotArea);
      float intRadius = pow((*m_event->m_singleScatter)->x_cm,2) + pow((*m_event->m_singleScatter)->y_cm,2);
      intRadius = sqrt(intRadius);
      driftTime_ns = (float)((*m_event->m_singleScatter)->driftTime_ns);
      float driftTime_us = driftTime_ns/1000.;

      // require S2 is fiducial in R and isn't a gas event
      if (m_cutsAW_DriftTimeETrainStudy->CX_FiducialVolumeR(intRadius, driftTime_us)){
        m_hists->BookFillHist("h_logProgenitorS2Area_fidcut; Log10(S2 Area [phd])",200,1.,7.,logProgArea);
        m_hists->BookFillHist("h_x_vs_y_fidcut; x [cm]; y [cm]",200,-100.,100.,200,-100.,100.,x_cm,y_cm);
        if (!m_cutsAW_DriftTimeETrainStudy->AboveAnode(s1TBA, s2TBA)) {
          m_hists->BookFillHist("h_logProgenitorS2Area_aacut; Log10(S2 Area [phd])",200,1.,7.,logProgArea);
          m_hists->BookFillHist("h_x_vs_y_aacut; x [cm]; y [cm]",200,-100.,100.,200,-100.,100.,x_cm,y_cm);
          // drop S2s following another large S2 within the a veto window 
          if (m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(last_s2_start,abs_aft1_time) > vetoWindow) {
            m_hists->BookFillHist("h_logProgenitorS2Area_dTcut; Log10(S2 Area [phd])",200,1.,7.,logProgArea);
            m_hists->BookFillHist("h_x_vs_y_dTcut; x [cm]; y [cm]",200,-100.,100.,200,-100.,100.,x_cm,y_cm);
            // passes all cuts! save :)
            save = true;
            m_hists->BookFillHist("h_prog_drift_times; Progenitor Drift Time [ns]",200,0., 10.e5, driftTime_ns);
            m_hists->BookFillHist("h_logProgenitorS2Area_allcuts; Log10(S2 Area [phd])",200,1.,7.,logProgArea);
            m_hists->BookFillHist("h_linProgenitorS2Area_allcuts; S2 Pulse Area [phd]",200,0.,pow(10,7.),pulseArea);
            m_hists->BookFillHist("h_x_vs_y_allcuts; x [cm]; y [cm]",200,-100.,100.,200,-100.,100.,x_cm,y_cm);
          }
        }
      }
    }
    isolated_s2_aft1_times.push_back(abs_aft1_time);
    isolated_s2_s1_aft1_times.push_back(s1_abs_aft1_time);
    isolated_s2_drift_times.push_back(driftTime_ns);
    isolated_s2_pulse_areas.push_back(pulseArea);
    isolated_s2_aft99_times.push_back(abs_aft99_time);
    isolated_s2_xs.push_back(x_cm);
    isolated_s2_ys.push_back(y_cm);
    isolated_s2_xs_centroid.push_back(x_cm_centroid);
    isolated_s2_ys_centroid.push_back(y_cm_centroid);
    isolated_s2_flags.push_back(save);
    last_s2_start = abs_aft1_time; 
    // try to do some map stuff
    for ( const auto &kv : this_evt_nveto_start ) {
      int tw = kv.first;
      if (dt_to_start < tw * pow(10,3)){
        this_evt_nveto_start[tw] ++;
      }
      if (dt_to_end < tw * pow(10,3)){
        this_evt_nveto_end[tw] ++;
      }
    }
  }

  // calculate ratio of first vs last
  for ( const auto &kv : ratios ) {
    int tw = kv.first;
    ratios[tw] = -1.;
    ratios_npulse[tw] = -1.;
    ratios_npulse_sig[tw] = -1.;
    if (prev_evt_sum_pa_end[tw] > 1){
      ratios[tw] = this_evt_sum_pa_start[tw]/prev_evt_sum_pa_end[tw];
      ratios_npulse[tw] = (float)this_evt_npulse_start[tw]/(float)prev_evt_npulse_end[tw];
      float frac_err        = sqrt(pow(1/sqrt(this_evt_sum_pa_start[tw]),2) + pow(1/sqrt(prev_evt_sum_pa_end[tw]),2));
      float frac_err_npulse = sqrt(pow(1/sqrt(this_evt_npulse_start[tw]),2) + pow(1/sqrt(prev_evt_npulse_end[tw]),2));
      ratios_sig[tw]        = 1/frac_err_npulse;
      ratios_npulse_sig[tw] = 1/frac_err_npulse;
    }
  }
  bool ghost_s2 = false;
  if ( ratios_npulse[1000] > 2 && this_evt_nveto_start[1000] == 0 && prev_evt_nveto_end[1000] == 0){
    ghost_s2 = true;
    // std::cout << "\nFound ghost S2?" << std::endl;
    // std::cout << "Ratio " << ratios_npulse[1000] << ", Num: " << this_evt_npulse_start[1000] << ", Den: " << prev_evt_npulse_end[1000] << std::endl;
    // std::cout << "Ratio pulse area " << ratios[1000] << ", Num: " << this_evt_sum_pa_start[1000] << ", Den: " << prev_evt_sum_pa_end[1000] << std::endl;
    // std::cout << "RunID " << runID << ", Event ID " << eventID << std::endl;
    // std::cout << "This event: " << this_evt_nveto_start[1000] << ", Prev: " << prev_evt_nveto_end[1000] << std::endl;
    // ghost_s2 = true;
  }

  if ( ratios[1000] > 20){
    // std::cout << "\nFound ghost S2 by pulse area?" << std::endl;
    // std::cout << "Ratio " << ratios_npulse[1000] << ", Num: " << this_evt_npulse_start[1000] << ", Den: " << prev_evt_npulse_end[1000] << std::endl;
    // std::cout << "Ratio pulse area " << ratios[1000] << ", Num: " << this_evt_sum_pa_start[1000] << ", Den: " << prev_evt_sum_pa_end[1000] << std::endl;
    // std::cout << "RunID " << runID << ", Event ID " << eventID << std::endl;
    // std::cout << "This event: " << this_evt_nveto_start[1000] << ", Prev: " << prev_evt_nveto_end[1000] << std::endl;
  }

  // flush out progenitor list
  // delete any pulses which are greater than 20 s old
  std::vector < int > to_delete;
  for (int i = 0; i <  isolated_s2_aft1_times.size(); i++){
    unsigned long long deltaT_evt = m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(isolated_s2_aft1_times[i], evt_start);
    if (deltaT_evt > postS2Window) {
      to_delete.push_back(i);
    }
    else {
      m_hists->BookFillHist("deltaT_evt",200,0.,6000.*pow(10,6),deltaT_evt);
    }
  }
  
  std::sort(to_delete.begin(), to_delete.end());
  
  for (int i = to_delete.size() - 1; i >= 0; i--){
    isolated_s2_aft1_times.erase(isolated_s2_aft1_times.begin() + to_delete[i]);
    isolated_s2_s1_aft1_times.erase(isolated_s2_s1_aft1_times.begin() + to_delete[i]);
    isolated_s2_drift_times.erase(isolated_s2_drift_times.begin() + to_delete[i]);
    isolated_s2_pulse_areas.erase(isolated_s2_pulse_areas.begin() + to_delete[i]);
    isolated_s2_aft99_times.erase(isolated_s2_aft99_times.begin() + to_delete[i]);
    isolated_s2_flags.erase(isolated_s2_flags.begin() + to_delete[i]);
    isolated_s2_xs.erase(isolated_s2_xs.begin() + to_delete[i]);
    isolated_s2_ys.erase(isolated_s2_ys.begin() + to_delete[i]);
    isolated_s2_xs_centroid.erase(isolated_s2_xs_centroid.begin() + to_delete[i]);
    isolated_s2_ys_centroid.erase(isolated_s2_ys_centroid.begin() + to_delete[i]);
  }

  int num_isolated_s2 = isolated_s2_aft1_times.size();

  // set up for histograms
  int    nbinsx        = 1000;

  // log plot bounds
  double xlow          = -8.;
  double xhigh         = 1.;
  double log_bin_size  = (xhigh-xlow)/nbinsx;

  // linear plot bounds
  int    nbinsx_lin    = 1000;
  double xlow_lin      = 0.;
  double xhigh_lin     = 10000.e6; // 10 s
  double lin_bin_size  = (xhigh_lin-xlow_lin)/nbinsx_lin;

  // progenitor S2 bounds
  int    nbinsy        = 200;
  double ylow          = 1.;
  double yhigh         = 7.;
  double ylow_lin      = 0.;
  double yhigh_lin     = pow(10,7);
  double ylog_bin_size = (yhigh-ylow)/nbinsy;
  double ylin_bin_size = (yhigh_lin-ylow_lin)/nbinsy;


  // record time following interesting s2s
  // - no saturated buffers cut from Ryan
  // - veto time after a muon
  // Require at least one S2 to look at
  // No multiple scatters
  if ( (m_cutsAW_DriftTimeETrainStudy->GatherPodChannelInfo() == 0) &&
       (event_window_ns < daq_lv_ns_all) && 
       (m_cuts->sr1()->TPCMuonVeto()) &&
       (!ghost_s2)) {
    
    if ( num_isolated_s2 > 0 ) {
      // loop through isolated, interesting S2s in list
      for (unsigned int iS2 = 0; iS2 < isolated_s2_pulse_areas.size(); iS2++) {
        if (isolated_s2_flags[iS2] == false) 
          continue; 
        float progDriftTime  = isolated_s2_drift_times[iS2];
        float logProgArea    = TMath::Log10(isolated_s2_pulse_areas[iS2]);
        // Calculate where we start relative to S2
        // 0 is defined as AFT1 of S2
        unsigned long long evt_count_start = m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(isolated_s2_aft1_times[iS2], isolated_s2_aft99_times[iS2]);
        // Event Window end - S2 1 time
        unsigned long long evt_count_end   = m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(isolated_s2_aft1_times[iS2],evt_end);

        // If end S2 comes before the event window
        // Delta T start = Event Window start - S2 start
        if ( m_cuts->sr1()->CheckForNegativeDeltaT(evt_start, isolated_s2_aft99_times[iS2]) ) {
          evt_count_start = m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(isolated_s2_aft99_times[iS2],evt_start);
        }

        // Look at following S2. 
        if (iS2 < (isolated_s2_pulse_areas.size() - 1))
          // if it starts before the event window end
          if (m_cuts->sr1()->CheckForNegativeDeltaT(evt_end,isolated_s2_aft1_times[iS2+1]))
            // and is after the event start
            if (!m_cuts->sr1()->CheckForNegativeDeltaT(evt_start,isolated_s2_aft1_times[iS2+1])) {
              // then stop counting at the next S2
              evt_count_end = std::min(m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(isolated_s2_aft1_times[iS2], isolated_s2_aft1_times[iS2+1]),
                                       m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(isolated_s2_aft1_times[iS2], evt_end));
              // if there is an S1 part of a SS, stop counting there
              if (SS_S1_pulseID > 0) {
                // if S1 is after the S2 
                if (m_cuts->sr1()->CheckForNegativeDeltaT(isolated_s2_s1_aft1_times[iS2],isolated_s2_aft1_times[iS2]))
                  evt_count_end = std::min(evt_count_end, m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(isolated_s2_aft1_times[iS2], isolated_s2_s1_aft1_times[iS2] ));
              }
            }
            else {
              // If the next pulse starts before the event window start, then just skip this S2
              continue;
            }
        
        if (evt_count_start > evt_count_end)
          continue;
        // Fill histogram with acquisition period * acquisition counts relative to S2
        // Here we are filling the log(dT) histograms
        for (int ibin = 0 ; ibin < nbinsx; ibin++){
          double log_start            = xlow + ibin*log_bin_size; 
          double log_end              = xlow + (ibin+1)*log_bin_size; 
          double log_to_lin_start_s   = pow(10,log_start);
          double log_to_lin_end_s     = pow(10,log_end);
          double log_to_lin_start_ns  = log_to_lin_start_s*pow(10,9);
          double log_to_lin_end_ns    = log_to_lin_end_s*pow(10,9);
          double log_to_lin_dt        = log_to_lin_end_s-log_to_lin_start_s;

          // Bin overlaps some fraction with event window
          if ( !(log_to_lin_start_ns > evt_count_end) && !(evt_count_start > log_to_lin_end_ns)) {
            double frac_evt = 1.;
            double den = log_to_lin_end_ns-log_to_lin_start_ns;
            // If the acqu window is a subset of the bin 
            if (log_to_lin_start_ns < evt_count_start && log_to_lin_end_ns > evt_count_end)
              frac_evt = (evt_count_end-evt_count_start) / den;
            // If acqu window starts after the bin low edge
            // Ends before the bin high edge
            if ((log_to_lin_start_ns > evt_count_start) && (evt_count_end < log_to_lin_end_ns))
              frac_evt = (evt_count_end-log_to_lin_start_ns) / den;
            // If acqu window starts before the bin low edge
            // Ends after the bin high edge
            if ((log_to_lin_start_ns < evt_count_start) && (evt_count_end > log_to_lin_end_ns))
              frac_evt = (log_to_lin_end_ns-evt_count_start) / den;

            log_to_lin_dt *= frac_evt;
            double x_fill = log_start + log_bin_size/2.; // Fill in the middle of the bin

            // 
            // if (evt_count_start < pow(10,7)) {
            //   std::cout  << "Evt count: " << evt_count_start << std::endl;
            //   std::cout  << "Log bin: "   << ibin            << std::endl;
            //   std::cout  << "xfill: "     << x_fill          << std::endl;
            //   std::cout  << "dt: "        << log_to_lin_dt   << std::endl;
            // }
            
            if (ibin == 0)
              break;

            m_hists->BookFillHist("h_logDeltaT_acqu; Log10(S2-SE time [s]);",nbinsx,xlow,xhigh,x_fill,log_to_lin_dt);
            m_hists->BookFillHist("h_logDeltaT_acqu_vs_logProg; Log10(S2-SE time [s]);Log10(S2) [phd];",nbinsx,xlow,xhigh,nbinsy,ylow,yhigh,x_fill,logProgArea,log_to_lin_dt);
          }
        }

        for (int ibin = 0; ibin < nbinsx_lin; ibin++) {
          // Now fill linear version
          double lin_start_ns   = lin_bin_size*ibin;
          double lin_end_ns     = lin_bin_size*(ibin+1);
          double lin_start_s  = lin_start_s/pow(10,9);
          double lin_end_s    = lin_end_s/pow(10,9);
          double lin_dt = lin_bin_size/pow(10,9.); // in seconds
          // Bin overlaps some fraction with event window
          //          if (lin_bin_size*ibin >= evt_count_start && lin_bin_size*(ibin+1) < evt_count_end){
          if ( !(lin_start_ns > evt_count_end) && !(evt_count_start > lin_end_ns)) {
            double frac_evt = 1.;
            double den = lin_end_ns-lin_start_ns;

            // If the acqu window is a subset of the bin 
            if (lin_start_ns < evt_count_start && lin_end_ns > evt_count_end)
              frac_evt = (evt_count_end-evt_count_start) / den;
            if ((lin_start_ns > evt_count_start) && (evt_count_end < lin_end_ns))
              frac_evt = (evt_count_end-lin_start_ns) / den;
            if ((lin_start_ns < evt_count_start) && (evt_count_end > lin_end_ns))
              frac_evt = (lin_end_ns-evt_count_start) / den;
            
            lin_dt *= frac_evt;
            
            double x_fill = lin_start_ns+lin_bin_size/2.;

            // if (evt_count_start < pow(10,7)){
            //   std::cout << "Evt count: " << evt_count_start << ", " << evt_count_end << std::endl;
            //   std::cout << "Linear bin: " << ibin << std::endl;
            //   std::cout << "xfill: " << x_fill << std::endl;
            //   std::cout << "lin_dt: " << lin_dt << std::endl;
            // }
            m_hists->BookFillHist("h_linDeltaT_acqu; S2-SE time [ns];",nbinsx_lin,xlow_lin,xhigh_lin,x_fill,lin_dt);
            m_hists->BookFillHist("h_linDeltaT_acqu_vs_logProg; S2-SE time [ns];Log10(S2) [phd];",nbinsx_lin,xlow_lin,xhigh_lin,nbinsy,ylow,yhigh,x_fill,logProgArea,lin_dt);
          }
        }
      }

      // Loop through SEs in event
      int nSEs = (*m_event->m_tpcPulses)->singleElectronPulseIDs.size();
      for( int iP = 0; iP < nSEs; ++iP ){
        int SEID             = (*m_event->m_tpcPulses)->singleElectronPulseIDs[iP];
        int pulseTime_ns     = (*m_event->m_tpcPulses)->areaFractionTime1_ns[SEID] + (*m_event->m_tpcPulses)->pulseStartTime_ns[SEID];
        int SE_width         = (*m_event->m_tpcPulses)->areaFractionTime99_ns[SEID]-(*m_event->m_tpcPulses)->areaFractionTime1_ns[SEID];
        int SE_width_5_95         = (*m_event->m_tpcPulses)->areaFractionTime95_ns[SEID]-(*m_event->m_tpcPulses)->areaFractionTime5_ns[SEID];
        float this_pulseArea = (*m_event->m_tpcPulses)->pulseArea_phd[SEID];
        float this_x         = (*m_event->m_tpcPulses)->s2Xposition_cm[SEID];
        float this_y         = (*m_event->m_tpcPulses)->s2Yposition_cm[SEID];
        float this_x_centroid = (*m_event->m_tpcPulses)->topCentroidX_cm[SEID];
        float this_y_centroid = (*m_event->m_tpcPulses)->topCentroidY_cm[SEID];
        std::pair<unsigned long,unsigned long> this_pulse_time = m_cuts->sr1()->GenerateFullPulseTime(trig_s,trig_ns,pulseTime_ns);
        unsigned long long deltaT = 0;
        // Loop through progenitor S2s
        // Assume time-ordered, so start from the back
        for (int iprog = isolated_s2_pulse_areas.size()-1; iprog >= 0; iprog--){
          // If following a non-isolated S2 pulse, break loop and skip this SE
          if (isolated_s2_flags[iprog] == false) {
            if (m_cuts->sr1()->CheckForNegativeDeltaT(this_pulse_time,isolated_s2_aft1_times[iprog]))
              break;
          }
          float logProgArea = TMath::Log10(isolated_s2_pulse_areas[iprog]);
          float progDriftTime = isolated_s2_drift_times[iprog];
          float progDriftTime_us = isolated_s2_drift_times[iprog]/pow(10,3);
          float wgt_scaleS2  = 1 / isolated_s2_pulse_areas[iprog];
          float etime_factor = 1/(TMath::Exp(progDriftTime_us/elifetime_us)-1);
          // Compute time
          deltaT = m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(isolated_s2_aft1_times[iprog],this_pulse_time);
          // If pulse is before S2, continue (can probably just break?)
          if( m_cuts->sr1()->CheckForNegativeDeltaT(isolated_s2_aft1_times[iprog],this_pulse_time) ) 
            continue;
          // look at following S2
          // if SE is in between S1 and S2 of the following progenitor S2, don't use!
          if ((iprog+1) <isolated_s2_pulse_areas.size())
            if (m_cuts->sr1()->CheckForNegativeDeltaT(this_pulse_time,isolated_s2_s1_aft1_times[iprog+1]) 
                && (m_cuts->sr1()->CheckForNegativeDeltaT(isolated_s2_aft1_times[iprog+1],this_pulse_time)))
              break;
          double logDeltaT = TMath::Log10((double)deltaT/pow(10,9));
          // Make radial cut 
          double dX = this_x-isolated_s2_xs[iprog];
          double dY = this_y-isolated_s2_ys[iprog];
          float dR = sqrt(dX*dX+dY*dY);
          float dR_centroid = sqrt(pow(this_x_centroid-isolated_s2_xs_centroid[iprog],2) + pow(this_y_centroid-isolated_s2_ys_centroid[iprog],2));
          //if (dR > maxdR_cm) break;
          // Histogram counts
          m_hists->BookFillHist("h_se_width; SE width [ns];Pulses;",100,0.,10000.,SE_width);     
          m_hists->BookFillHist("h_se_width_5_95; SE width [ns];Pulses;",100,0.,10000.,SE_width_5_95);     
          m_hists->BookFillHist("h_SE_pa; Pulse Area [phd]; SE Pulses;",300,0.,150., this_pulseArea);    
          m_hists->BookFillHist("h_SE_dR; #Delta R; SE Pulses;",300,0.,50.,dR);    
          m_hists->BookFillHist("h_SE_dR_centroid; #Delta R (centroid); SE Pulses;",300,0.,50.,dR_centroid);    
          m_hists->BookFillHist("h_logDeltaT_SE_counts_nowgt; Log10(S2-SE time [s]);",nbinsx,xlow,xhigh,logDeltaT);    
          m_hists->BookFillHist("h_logDeltaT_SE_counts_wgt_scaleS2; Log10(S2-SE time [s]);",nbinsx,xlow,xhigh,logDeltaT, wgt_scaleS2);
          m_hists->BookFillHist("h_logDeltaT_SE_counts_wgt_scaleS2_etime_factor; Log10(S2-SE time [s]);",nbinsx,xlow,xhigh,logDeltaT, wgt_scaleS2*etime_factor);
          m_hists->BookFillHist("h_linDeltaT_SE_counts_nowgt; S2-SE time [ns];",nbinsx_lin,xlow_lin,xhigh_lin,deltaT);    
          m_hists->BookFillHist("h_linDeltaT_SE_counts_wgt_scaleS2; S2-SE time [ns];",nbinsx_lin,xlow_lin,xhigh_lin,deltaT, wgt_scaleS2);
          m_hists->BookFillHist("h_linDeltaT_SE_counts_wgt_scaleS2_etime_factor; S2-SE time [ns];",nbinsx_lin,xlow_lin,xhigh_lin,deltaT,  wgt_scaleS2*etime_factor);
          // 2D histograms
          m_hists->BookFillHist("h_deltaT_logProgenitorArea; S2-SE time [ns]; Log10(S2 Area [phd])",nbinsx_lin,xlow_lin,xhigh_lin,200,1.,7.,deltaT,logProgArea);
          m_hists->BookFillHist("h_logDeltaT_logProgenitorArea_nowgt; Log10(S2-SE time [s]); Log10(S2 Area [phd])",nbinsx,xlow,xhigh,200,1.,7.,logDeltaT,logProgArea);    
          if ( (fabs(dX) < 10) && (fabs(dY) < 10)){
            m_hists->BookFillHist("h_deltaT_logProgenitorArea_dX; S2-SE time [ns]; Log10(S2 Area [phd])",nbinsx_lin,xlow_lin,xhigh_lin,200,1.,7.,deltaT,logProgArea);
            m_hists->BookFillHist("h_logDeltaT_logProgenitorArea_dX_nowgt; Log10(S2-SE time [s]); Log10(S2 Area [phd])",nbinsx,xlow,xhigh,200,1.,7.,logDeltaT,logProgArea);
          }
          m_hists->BookFillHist("h_logDeltaT_pulseAreaSE_nowgt; Log10(S2-SE time [s]); SE Area [phd]",nbinsx,xlow,xhigh,200,0.,5000.,logDeltaT,this_pulseArea);
          m_hists->BookFillHist("h_logDeltaT_dR_SE_nowgt; Log10(Prog. S2-S2 time [s]); #deltaR [cm]",nbinsx,xlow,xhigh,1000,0.,100.,logDeltaT,dR);    
          m_hists->BookFillHist("h_logDeltaT_dR_centroid_SE_nowgt; Log10(Prog. S2-S2 time [s]); #deltaR [cm]",nbinsx,xlow,xhigh,1000,0.,100.,logDeltaT,dR_centroid);
          m_hists->BookFillHist("h_dX_vs_dY_nowgt; #Deltax [cm]; #Deltay [cm]",500,-100.,100.,500,-100.,100.,dX,dY);
          m_hists->BookFillHist("h_SE_x_vs_y_nowgt; SE x [cm]; SE y [cm]",500,-100.,100.,500,-100.,100.,this_x,this_y);
          m_hists->BookFillHist("h_SE_x_vs_y_centroid_nowgt; SE centroid x [cm]; SE centroid y [cm]",500,-100.,100.,500,-100.,100.,this_x_centroid,this_y_centroid);

          // if we get here:
          break;
        }
      }

      // Loop through S2s  in event
      int nS2s = (*m_event->m_tpcPulses)->s2PulseIDs.size();
      for( int iP = 0; iP < nS2s; ++iP ){
        int S2ID             = (*m_event->m_tpcPulses)->s2PulseIDs[iP];
        int pulseTime_ns     = (*m_event->m_tpcPulses)->areaFractionTime1_ns[S2ID] + (*m_event->m_tpcPulses)->pulseStartTime_ns[S2ID];
        int S2_width         = (*m_event->m_tpcPulses)->areaFractionTime99_ns[S2ID]-(*m_event->m_tpcPulses)->areaFractionTime1_ns[S2ID];
        float this_pulseArea = (*m_event->m_tpcPulses)->pulseArea_phd[S2ID];
        float this_x         = (*m_event->m_tpcPulses)->s2Xposition_cm[S2ID];
        float this_y         = (*m_event->m_tpcPulses)->s2Yposition_cm[S2ID];
        float this_r         = sqrt(this_x*this_x+this_y*this_y);
        float this_x_centroid = (*m_event->m_tpcPulses)->topCentroidX_cm[S2ID];
        float this_y_centroid = (*m_event->m_tpcPulses)->topCentroidY_cm[S2ID];

        std::pair <unsigned long, unsigned long> abs_aft1_time = m_cuts->sr1()->GenerateFullPulseTime(trig_s,trig_ns,pulseTime_ns);
        unsigned long long dt_to_end   = m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(abs_aft1_time, evt_end);
        unsigned long long dt_to_start = m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(evt_start, abs_aft1_time);
        // exclude any possible progenitor S2s
        if (this_pulseArea > progenitorThresh)
          continue;
        // Don't count S2s that are a part of the SS or MS
        if (S2ID == SS_S2_pulseID)
          continue;
        bool skip_MS = false;
        for (int iMS = 0; iMS < MS_S2_pulseIDs.size(); iMS++)
          if (S2ID == MS_S2_pulseIDs[iMS])
            skip_MS = true;
        if (skip_MS) continue;
        
        std::pair<unsigned long,unsigned long> this_pulse_time = m_cuts->sr1()->GenerateFullPulseTime(trig_s,trig_ns,pulseTime_ns);
        unsigned long long deltaT = 0;
        // Loop through progenitor S2s
        // Assume time-ordered, so start from the back
        for (int iprog = isolated_s2_pulse_areas.size()-1; iprog >= 0; iprog--){
          // If following a non-isolated S2 pulse, break loop and skip this S2
          if (isolated_s2_flags[iprog] == false) {
            if (m_cuts->sr1()->CheckForNegativeDeltaT(this_pulse_time,isolated_s2_aft1_times[iprog]))
              break;
          }
          float logProgArea = TMath::Log10(isolated_s2_pulse_areas[iprog]);
          float progDriftTime    = isolated_s2_drift_times[iprog];
          float progDriftTime_us = isolated_s2_drift_times[iprog]/pow(10,3);
          float wgt_scaleS2      = 1 / isolated_s2_pulse_areas[iprog];
          float etime_factor     = 1/(TMath::Exp(progDriftTime_us/elifetime_us)-1);
          float wgt_pulseArea    = this_pulseArea/pulseArea_SE; 
          // Compute time
          deltaT = m_cutsAW_DriftTimeETrainStudy->ComputeDeltaT(isolated_s2_aft1_times[iprog],this_pulse_time);
          if( m_cuts->sr1()->CheckForNegativeDeltaT(isolated_s2_aft1_times[iprog],this_pulse_time) ) continue;
          // look at following S2
          // if S2 is in between S1 and S2 of the following progenitor S2, don't use!
          if ((iprog+1) <isolated_s2_pulse_areas.size())
            if (m_cuts->sr1()->CheckForNegativeDeltaT(this_pulse_time,isolated_s2_s1_aft1_times[iprog+1]) 
                && (m_cuts->sr1()->CheckForNegativeDeltaT(isolated_s2_aft1_times[iprog+1],this_pulse_time)))
              break;
          // Make radial cut 
          double dX = this_x-isolated_s2_xs[iprog];
          double dY = this_y-isolated_s2_ys[iprog];
          float dR = sqrt(pow(this_x-isolated_s2_xs[iprog],2) + pow(this_y-isolated_s2_ys[iprog],2));
          float dR_centroid = sqrt(pow(this_x_centroid-isolated_s2_xs_centroid[iprog],2) + pow(this_y_centroid-isolated_s2_ys_centroid[iprog],2));
          //if (dR > maxdR_cm) break;
          double logDeltaT = TMath::Log10((double)deltaT/pow(10,9));
          
          
          // Histogram counts
          m_hists->BookFillHist("h_s2_width; S2 width [ns];Pulses;",100,0.,10000.,S2_width);    
          m_hists->BookFillHist("h_S2_pa; Pulse Area [phd]; S2 Pulses;",5000,0.,5000., this_pulseArea);    
          m_hists->BookFillHist("h_logDeltaT_S2_counts_nowgt; Log10(Prog. S2-S2 time [s]);",nbinsx,xlow,xhigh,logDeltaT);    
          m_hists->BookFillHist("h_dX_vs_dY_S2_nowgt; #Deltax [cm]; #Deltay [cm]",500,-100.,100.,500,-100.,100.,dX,dY);
          m_hists->BookFillHist("h_S2_x_vs_y_nowgt; S2 x [cm]; S2 y [cm]",500,-100.,100.,500,-100.,100.,this_x,this_y);
          m_hists->BookFillHist("h_S2_x_vs_y_centroid_nowgt; S2 centroid x [cm]; S2 centroid y [cm]",500,-100.,100.,500,-100.,100.,this_x_centroid,this_y_centroid);

          if ( deltaT > 10*pow(10,6) ){
            m_hists->BookFillHist("h_s2_width_dt_cut; S2 width [ns];Pulses;",100,0.,10000.,S2_width);    
            m_hists->BookFillHist("h_S2_pa_dt_cut; Pulse Area [phd]; S2 Pulses;",5000,0.,5000., this_pulseArea);    
            m_hists->BookFillHist("h_S2_x_vs_y_dt_cut_nowgt; S2 x [cm]; S2 y [cm]",500,-100.,100.,500,-100.,100.,this_x,this_y);
            m_hists->BookFillHist("h_S2_pa_dX_dt_cut; Pulse Area [phd]; #DeltaX [cm]",5000,0.,5000.,1000,0.,100.,this_pulseArea,dX);    
            m_hists->BookFillHist("h_S2_pa_dY_dt_cut; Pulse Area [phd]; #DeltaY [cm]",5000,0.,5000.,1000,0.,100.,this_pulseArea,dY);    
            m_hists->BookFillHist("h_S2_pa_x_dt_cut; Pulse Area [phd]; #DeltaX [cm]",5000,0.,5000.,500,100.,100.,this_pulseArea,this_x);    
            m_hists->BookFillHist("h_S2_pa_y_dt_cut; Pulse Area [phd]; #DeltaY [cm]",5000,0.,5000.,500,-100.,100.,this_pulseArea,this_y);    
          }
          
          if ( (fabs(dX) < 10) && (fabs(dY) < 10))
            m_hists->BookFillHist("h_linDeltaT_S2_counts_dX_nowgt; Prog. S2-S2 time [ns];",nbinsx_lin,xlow_lin,xhigh_lin,deltaT);
          m_hists->BookFillHist("h_logDeltaT_S2_counts_wgt_scaleS2; Log10(Prog. S2-S2 time [s]);",nbinsx,xlow,xhigh,logDeltaT, wgt_scaleS2);
          m_hists->BookFillHist("h_logDeltaT_S2_counts_wgt_scaleS2_etime_factor; Log10(Prog. S2-S2 time [s]);",nbinsx,xlow,xhigh,logDeltaT, wgt_scaleS2*etime_factor);
          m_hists->BookFillHist("h_logDeltaT_S2_counts_wgt_scaleS2_pa; Log10(Prog. S2-S2 time [s]);",nbinsx,xlow,xhigh,logDeltaT, wgt_scaleS2*wgt_pulseArea);
          m_hists->BookFillHist("h_linDeltaT_S2_counts_nowgt; Prog. S2-S2 time [ns];",nbinsx_lin,xlow_lin,xhigh_lin,deltaT);    
          m_hists->BookFillHist("h_linDeltaT_pulseAreaS2_nowgt; Prog. S2-S2 time [ns]; S2 Area [phd]",nbinsx_lin,xlow_lin,xhigh_lin,5000,0.,5000.,deltaT,this_pulseArea);    
          for ( const auto &kv : ratios ) {
            int tw = kv.first;
            m_hists->BookFillHist(Form("h_linDeltaT_ratio_%dus_nowgt; Prog. S2-S2 time [ns]; First 50 #us Last 50 #us pulse area",tw),nbinsx_lin,xlow_lin,xhigh_lin,500,-1.5,1000.,deltaT,ratios[tw]);    
            m_hists->BookFillHist(Form("h_linDeltaT_ratio_%dus_zoom_nowgt; Prog. S2-S2 time [ns]; First 50 #us Last 50 #us pulse area",tw),nbinsx_lin,xlow_lin,xhigh_lin,500,-1.5,10.,deltaT,ratios[tw]);    
          }
          for ( const auto &kv : ratios_npulse ) {
            int tw = kv.first;
            m_hists->BookFillHist(Form("h_linDeltaT_ratio_npulse_%dus_nowgt; Prog. S2-S2 time [ns]; First 50 #us Last 50 #us N(pulse)",tw),nbinsx_lin,xlow_lin,xhigh_lin,500,-1.5,1000.,deltaT,ratios_npulse[tw]);    
            m_hists->BookFillHist(Form("h_linDeltaT_ratio_npulse_%dus_zoom_nowgt; Prog. S2-S2 time [ns]; First 50 #us Last 50 #us N(pulse)",tw),nbinsx_lin,xlow_lin,xhigh_lin,500,-1.5,10.,deltaT,ratios_npulse[tw]);    
            m_hists->BookFillHist(Form("h_ratio_pa_vs_ratio_npulse_%dus_zoom_nowgt; First 50 #us Last 50 #us pulse area ; First 50 #us Last 50 #us N(pulse)",tw),500,-1.5,10.,500,-1.5,10.,ratios[tw],ratios_npulse[tw]);    
            m_hists->BookFillHist(Form("h_ratio_pa_vs_ratio_npulse_%dus_nowgt; First 50 #us Last 50 #us pulse area ; First 50 #us Last 50 #us N(pulse)",tw),500,-1.5,1000.,500,-1.5,1000.,ratios[tw],ratios_npulse[tw]);    
            m_hists->BookFillHist(Form("h_ratio_npulse_sig_vs_ratio_npulse_%dus_nowgt; First 50 #us Last 50 #us N(pulse) sig. ; First 50 #us Last 50 #us N(pulse)",tw),500,-1.5,1000.,500,-1.5,1000.,ratios_npulse_sig[tw],ratios_npulse[tw]);    
          }
          for ( const auto &kv : ratios_npulse_sig ) {
            int tw = kv.first;
            m_hists->BookFillHist(Form("h_linDeltaT_ratio_npulse_sig_%dus_nowgt; Prog. S2-S2 time [ns]; First 50 #us Last 50 #us N(pulse) sig.",tw),nbinsx_lin,xlow_lin,xhigh_lin,500,-1.5,1000.,deltaT,ratios_npulse_sig[tw]);    
            m_hists->BookFillHist(Form("h_linDeltaT_ratio_npulse_sig_%dus_zoom_nowgt; Prog. S2-S2 time [ns]; First 50 #us Last 50 #us N(pulse) sig.",tw),nbinsx_lin,xlow_lin,xhigh_lin,500,-1.5,10.,deltaT,ratios_npulse_sig[tw]);    
          }
          m_hists->BookFillHist("h_linDeltaT_S2_counts_wgt_scaleS2; Prog. S2-S2 time [ns];",nbinsx_lin,xlow_lin,xhigh_lin,deltaT, wgt_scaleS2);
          m_hists->BookFillHist("h_linDeltaT_S2_counts_wgt_scaleS2_etime_factor; Prog. S2-S2 time [ns];",nbinsx_lin,xlow_lin,xhigh_lin,deltaT,  wgt_scaleS2*etime_factor);
          m_hists->BookFillHist("h_linDeltaT_S2_counts_wgt_scaleS2_pa; Prog. Prog. S2-S2 time [ns];",nbinsx_lin,xlow_lin,xhigh_lin,deltaT,  wgt_scaleS2*wgt_pulseArea);
          // 2D histograms
          m_hists->BookFillHist("h_deltaT_logProgenitorArea; Prog. S2-S2 time [ns]; Log10(S2 Area [phd])",nbinsx_lin,xlow_lin,xhigh_lin,200,1.,7.,deltaT,logProgArea);
          m_hists->BookFillHist("h_deltaT_S2_width; Prog. S2-S2 time [ns]; S2 width [ns]",nbinsx_lin,xlow_lin,xhigh_lin,100,0.,10000.,deltaT,S2_width);
          m_hists->BookFillHist("h_logDeltaT_logProgenitorArea_nowgt; Log10(Prog. S2-S2 time [s]); Log10(S2 Area [phd])",nbinsx,xlow,xhigh,200,1.,7.,logDeltaT,logProgArea);    
          m_hists->BookFillHist("h_deltaT_driftTime_nowgt; Prog. S2-S2 time [ns]; Drift time [ns]",nbinsx_lin,xlow_lin,xhigh_lin,200,-2,10e5,deltaT,progDriftTime);
          m_hists->BookFillHist("h_deltaT_driftTime_wgt; Prog. S2-S2 time [ns]; Drift time [ns]",nbinsx_lin,xlow_lin,xhigh_lin,200,-2,10e5,deltaT,progDriftTime,this_pulseArea);
          m_hists->BookFillHist("h_logDeltaT_pulseAreaS2_nowgt; Log10(Prog. S2-S2 time [s]); S2 Area [phd]",nbinsx,xlow,xhigh,200,0.,5000.,logDeltaT,this_pulseArea);    
          m_hists->BookFillHist("h_logDeltaT_dR_S2_nowgt; Log10(Prog. S2-S2 time [s]); #DeltaR [cm]",nbinsx,xlow,xhigh,1000,0.,100.,logDeltaT,dR);    
          m_hists->BookFillHist("h_logDeltaT_dX_S2_nowgt; Log10(Prog. S2-S2 time [s]); #DeltaX [cm]",nbinsx,xlow,xhigh,1000,0.,100.,logDeltaT,dX);    
          m_hists->BookFillHist("h_logDeltaT_dY_S2_nowgt; Log10(Prog. S2-S2 time [s]); #DeltaX [cm]",nbinsx,xlow,xhigh,1000,0.,100.,logDeltaT,dY);    
          m_hists->BookFillHist("h_logDeltaT_dR_centroid_S2_nowgt; Log10(Prog. S2-S2 time [s]); #DeltaR [cm]",nbinsx,xlow,xhigh,1000,0.,100.,logDeltaT,dR_centroid);

          // test radial cut
          if (this_r < 50){
            m_hists->BookFillHist("h_deltaT_S2_width_rl50; Prog. S2-S2 time [ns]; S2 width [ns]",nbinsx_lin,xlow_lin,xhigh_lin,100,0.,10000.,deltaT,S2_width);
            m_hists->BookFillHist("h_linDeltaT_pulseAreaS2_rl50_nowgt; Prog. S2-S2 time [ns]; S2 Area [phd]",nbinsx_lin,xlow_lin,xhigh_lin,5000,0.,5000.,deltaT,this_pulseArea);    
          }
          // progenitor pulse areas
          for (int ij = 0; ij < s2_log_pa_slices.size(); ij++){
            double pa_low = s2_log_pa_slices[ij].first;
            double pa_high = s2_log_pa_slices[ij].second; 
            if ((logProgArea > pa_low) && (logProgArea < pa_high)){ 
              m_hists->BookFillHist(Form("h_logDeltaT_S2_counts_logprog_slice%d_nowgt; Log10(Prog. S2-S2 time [s]);",ij),nbinsx,xlow,xhigh,logDeltaT);    
              m_hists->BookFillHist(Form("h_logDeltaT_S2_counts_logprog_slice%d_wgt_pa; Log10(Prog. S2-S2 time [s]);",ij),nbinsx,xlow,xhigh,logDeltaT,wgt_pulseArea);
              m_hists->BookFillHist(Form("h_linDeltaT_S2_counts_logprog_slice%d_nowgt; Prog. S2-S2 time [ns];",ij),nbinsx_lin,xlow_lin,xhigh_lin,deltaT);    
              m_hists->BookFillHist(Form("h_linDeltaT_S2_counts_logprog_slice%d_wgt_pa; Prog. S2-S2 time [ns];",ij),nbinsx_lin,xlow_lin,xhigh_lin,deltaT,wgt_pulseArea);
            }
          }
          // if we get here:
          break;
        }
      }
    }
  }

  for ( const auto &kv : prev_evt_sum_pa_start ) {
    int tw = kv.first;
    prev_evt_sum_pa_start[tw] = this_evt_sum_pa_start[tw];
    this_evt_sum_pa_start[tw] = 0;
    prev_evt_sum_pa_end[tw] = this_evt_sum_pa_end[tw];
    this_evt_sum_pa_end[tw] = 0;
    prev_evt_npulse_start[tw] = this_evt_npulse_start[tw];
    this_evt_npulse_start[tw] = 0;
    prev_evt_npulse_end[tw] = this_evt_npulse_end[tw];
    this_evt_npulse_end[tw] = 0;
    prev_evt_nveto_start[tw] = this_evt_nveto_start[tw];
    this_evt_nveto_start[tw] = 0;
    prev_evt_nveto_end[tw] = this_evt_nveto_end[tw];
    this_evt_nveto_end[tw] = 0;
  }

}

// Finalize() - Called once after event loop.
void AW_DriftTimeETrainStudy::Finalize()
{
    INFO("Finalizing AW_DriftTimeETrainStudy Analysis");
}
